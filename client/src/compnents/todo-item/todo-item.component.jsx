import "./todo-item.styles.css";

import { getUserPicture } from "../../utils/user.utils";

const TodoItem = ({
  id,
  title,
  description,
  assignedToUser,
  handleDragStart,
  handleDragEnd,
  handleMouseMove,
}) => {
  return (
    <div
      id={id}
      className="todo-item flex direction-column"
      draggable
      onDragStart={handleDragStart}
      onDragEnd={handleDragEnd}
      onMouseMove={handleMouseMove}
    >
      <div>
        <h4>{title}</h4> <p>{description}</p>
      </div>
      <div className="divider"></div>
      <div className="user-info flex">
        {assignedToUser}
        <img
          src={getUserPicture(assignedToUser)}
          alt="User"
          className="circle"
        />
      </div>
    </div>
  );
};

export default TodoItem;
