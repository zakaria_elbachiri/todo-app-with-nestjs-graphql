import { useState } from "react";

import "./custom-input.styles.css";

const CustomInput = ({ type, placeholder, id, name, handleChange }) => {
  const [active, setActive] = useState(false);

  const handleFocus = () => setActive(true),
    handleBlur = ({ target: { value } }) => setActive(value !== "");

  return (
    <div
      className={`custom-input ${active ? "active" : ""}`}
      onFocus={handleFocus}
      onBlur={handleBlur}
    >
      {type ? (
        <input id={id} name={name} type={type} onChange={handleChange} />
      ) : (
        <textarea
          id={id}
          name={name}
          rows="3"
          wrap="soft"
          onChange={handleChange}
        ></textarea>
      )}
      <label className="bold" htmlFor={id}>
        {placeholder}
      </label>
    </div>
  );
};

export default CustomInput;
