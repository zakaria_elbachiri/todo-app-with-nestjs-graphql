import "./todo-list.component.css";

import { useState } from "react";

import NewTodo from "../new-todo/new-todo.component";

import addIcon from "../../images/add.png";

const TodoList = ({
  id,
  title,
  count,
  children,
  handleDragOver,
  handleDrop,
  addButton,
}) => {
  const [toggleNewTodoForm, setToggleNewTodoForm] = useState(false);

  const handleClick = () => setToggleNewTodoForm((prevValue) => !prevValue);

  return (
    <div
      id={id}
      className="Todo-list flex direction-column"
      onDragOver={handleDragOver}
      onDrop={handleDrop}
    >
      <div className="heading grid bold">
        <h3>{title}</h3>
        <span className="count flex color-white circle">{count}</span>
        {addButton && (
          <input
            type="image"
            src={addIcon}
            alt="Add icon"
            onClick={handleClick}
          />
        )}
      </div>
      {toggleNewTodoForm && <NewTodo />}
      {children}
    </div>
  );
};

export default TodoList;
