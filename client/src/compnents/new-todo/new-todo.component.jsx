import "./new-todo.styles.css";

import { useQuery, useMutation } from "@apollo/client";

import { GET_USERS } from "../../graphql/queries/user.query";
import { GET_TODOS_BY_TYPE } from "../../graphql/queries/todo.query";
import { CREATE_TODO } from "../../graphql/mutations/todo.mutation";

import CustomInput from "../custom-input/custom-input.component";
import { Children, useState } from "react";

const NewTodo = () => {
  const { data } = useQuery(GET_USERS);
  const [mutateCreateTodo] = useMutation(CREATE_TODO, {
    refetchQueries: [GET_TODOS_BY_TYPE],
  });

  const [todo, setTodo] = useState({});

  const handleChange_inputs = ({ target: { value, name } }) =>
      setTodo((prevValue) => ({ ...prevValue, [name]: value })),
    handleChange_select = ({ target: { value, name } }) =>
      setTodo((prevValue) => ({
        ...prevValue,
        [name]: value,
      })),
    handleClick = () => {
      if (!todo.title || !todo.assignedToUser) return;
      mutateCreateTodo({
        variables: {
          title: todo.title,
          description: todo.description,
          assignedToUser: todo.assignedToUser,
        },
      });
    };

  return (
    <form className="new-todo flex direction-column">
      <CustomInput
        type="text"
        placeholder="Title"
        id="title"
        name="title"
        handleChange={handleChange_inputs}
      />
      <CustomInput
        placeholder="Description"
        id="description"
        name="description"
        handleChange={handleChange_inputs}
      />
      <select name="assignedToUser" onChange={handleChange_select}>
        <option>Assign to :</option>
        {Children.toArray(
          data?.users.map((user) => (
            <option
              value={user._id}
            >{`${user.firstName} ${user.lastName}`}</option>
          ))
        )}
      </select>
      <button type="button" className="bold" onClick={handleClick}>
        Add
      </button>
    </form>
  );
};

export default NewTodo;
