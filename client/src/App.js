import "./App.css";

import { Children, useEffect, useState } from "react";
import { useMutation, useQuery } from "@apollo/client";

import { GET_TODOS_BY_TYPE } from "./graphql/queries/todo.query";
import { UPDATE_TODO } from "./graphql/mutations/todo.mutation";

import TodoList from "./compnents/todo-list/todo-list.component";
import TodoItem from "./compnents/todo-item/todo-item.component";

function App() {
  const [draggingObject, setDraagingObject] = useState({
    itemId: "",
    type: "",
  });

  const [mutateUpdateTodo] = useMutation(UPDATE_TODO, {
    refetchQueries: [GET_TODOS_BY_TYPE],
  });

  const { data: todo } = useQuery(GET_TODOS_BY_TYPE, {
      variables: {
        type: "todo",
      },
    }),
    { data: inProgress } = useQuery(GET_TODOS_BY_TYPE, {
      variables: {
        type: "inProgress",
      },
    }),
    { data: done } = useQuery(GET_TODOS_BY_TYPE, {
      variables: {
        type: "done",
      },
    });

  //Event handlers
  const handleDragStart = (event) => {
      event.dataTransfer.setData("text/html", event.target.outerHTML);
      event.dataTransfer.effectAllowed = "move";
      setDraagingObject((prevValue) => ({
        ...prevValue,
        itemId: event.target.id,
      }));
    },
    handleDragEnd = (event) => {
      if (event.dataTransfer.dropEffect !== "move" || !draggingObject.type)
        return;

      // Mutating the changes
      mutateUpdateTodo({
        variables: {
          _id: draggingObject.itemId,
          type: draggingObject.type,
        },
      });
    },
    handleDragOver = (event) => {
      if (!event.dataTransfer.types.includes("text/html")) return;
      event.preventDefault();
    },
    handleDrop = (event) => {
      event.preventDefault();

      setDraagingObject((prevValue) => ({
        ...prevValue,
        type: event.target.id,
      }));
    };

  return (
    <div className="App margin-center flex">
      <header>
        <h1>
          Just A <s>TODO</s> App <span>😅</span>
        </h1>
      </header>
      <main className="grid">
        <TodoList
          id="todo"
          title="To Do"
          count={todo?.todosByType?.length}
          addButton
          handleDragOver={handleDragOver}
          handleDrop={handleDrop}
        >
          {todo?.todosByType.length > 0 &&
            Children.toArray(
              todo.todosByType.map(
                ({ _id, title, description, assignedToUser }) => (
                  <TodoItem
                    id={_id}
                    title={title}
                    description={description}
                    assignedToUser={`${assignedToUser.firstName} ${assignedToUser.lastName}`}
                    handleDragStart={handleDragStart}
                    handleDragEnd={handleDragEnd}
                  />
                )
              )
            )}
        </TodoList>

        <TodoList
          id="inProgress"
          title="In progress"
          count={inProgress?.todosByType.length}
          handleDragOver={handleDragOver}
          handleDrop={handleDrop}
        >
          {inProgress?.todosByType.length > 0 &&
            Children.toArray(
              inProgress.todosByType.map(
                ({ _id, title, description, assignedToUser }) => (
                  <TodoItem
                    id={_id}
                    title={title}
                    description={description}
                    assignedToUser={`${assignedToUser.firstName} ${assignedToUser.lastName}`}
                    handleDragStart={handleDragStart}
                    handleDragEnd={handleDragEnd}
                  />
                )
              )
            )}
        </TodoList>

        <TodoList
          id="done"
          title="Done"
          count={done?.todosByType?.length}
          handleDragOver={handleDragOver}
          handleDrop={handleDrop}
        >
          {done?.todosByType.length > 0 &&
            Children.toArray(
              done.todosByType.map(
                ({ _id, title, description, assignedToUser }) => (
                  <TodoItem
                    id={_id}
                    title={title}
                    description={description}
                    assignedToUser={`${assignedToUser.firstName} ${assignedToUser.lastName}`}
                    handleDragStart={handleDragStart}
                    handleDragEnd={handleDragEnd}
                  />
                )
              )
            )}
        </TodoList>
      </main>
    </div>
  );
}

export default App;
