import user1 from "../images/user-1.jpg";
import user2 from "../images/user-2.jpg";
import user3 from "../images/user-3.jpg";

export const getUserPicture = (fullName) => {
  switch (fullName) {
    case "Zakaria El Bachiri":
      return user2;
    case "Hassan Dahbi":
      return user3;
    case "Mouad El Mhali":
      return user1;
    default:
      break;
  }
};
