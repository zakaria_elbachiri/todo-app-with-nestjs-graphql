import { gql } from "@apollo/client";

export const CREATE_TODO = gql`
  mutation CreateTodo(
    $title: String!
    $description: String
    $assignedToUser: String!
  ) {
    createTodo(
      createTodoInput: {
        title: $title
        description: $description
        assignedToUser: $assignedToUser
      }
    ) {
      _id
      title
    }
  }
`;

export const UPDATE_TODO = gql`
  mutation UpdateTodo($_id: String!, $type: String!) {
    updateTodo(updateTodoInput: { _id: $_id, type: $type }) {
      _id
    }
  }
`;
