import { gql } from "@apollo/client";

export const GET_TODOS_BY_TYPE = gql`
  query ($type: String!) {
    todosByType(type: $type) {
      _id
      title
      description
      type
      assignedToUser {
        _id
        firstName
        lastName
      }
    }
  }
`;
