# Todo App ✅

A very minimal todo app inspired by the "Confluence" platform.

## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/zakaria_elbachiri/todo-app-with-nestjs-graphql.git
```

Go to the project directory

```bash
  cd my-project
```

Install dependencies (client & server)

```bash
  npm run install
```

Start the app

```bash
  npm run start:client
```

Start the server

```bash
  npm run start:server
```

## Demo

- **Creating a new TODO ➕**
  https://gitlab.com/zakaria_elbachiri/todo-app-with-nestjs-graphql/-/blob/main/demo-new-todo.webm

- **Updating a TODO 🔃**
  https://gitlab.com/zakaria_elbachiri/todo-app-with-nestjs-graphql/-/blob/main/demo-update-todo.webm

## Authors

- [@zakaria_elbachiri](https://gitlab.com/zakaria_elbachiri)
