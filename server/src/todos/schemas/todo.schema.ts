import { Schema, Prop, SchemaFactory } from '@nestjs/mongoose';
import { Document, Types } from 'mongoose';

import { User } from '../../users/schemas/user.schema';

export type TodoDocument = Todo & Document;

@Schema()
export class Todo {
  @Prop({ type: Types.ObjectId })
  _id: string;

  @Prop({ required: true })
  title: string;

  @Prop()
  description: string;

  @Prop({ required: true })
  type: string;

  @Prop({ required: true, type: Types.ObjectId, ref: 'user' })
  assignedToUser: User;
}

export const TodoSchema = SchemaFactory.createForClass(Todo);
