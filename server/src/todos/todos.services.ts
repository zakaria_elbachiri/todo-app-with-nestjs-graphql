import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { Todo, TodoDocument } from './schemas/todo.schema';

@Injectable()
export class TodosService {
  constructor(@InjectModel(Todo.name) private todoModel: Model<TodoDocument>) {}

  async create(
    title: string,
    description: string,
    assignedToUser: string,
    type: string,
  ): Promise<Todo> {
    const todoInstance = this.todoModel.create({
      _id: new Types.ObjectId(),
      title,
      description,
      type,
      assignedToUser: new Types.ObjectId(assignedToUser),
    });

    return (await todoInstance).save();
  }

  async getAllTodos(): Promise<Todo[]> {
    return await this.todoModel.find();
  }

  async getTodoById(id: string): Promise<Todo> {
    return await this.todoModel.findById(new Types.ObjectId(id));
  }

  async getTodosByType(type: string): Promise<Todo[]> {
    return await this.todoModel.find({ type });
  }

  async updateTodo(id: string, type: string): Promise<Todo> {
    return await this.todoModel.findByIdAndUpdate(
      new Types.ObjectId(id),
      {
        type,
      },
      { new: true },
    );
  }

  async deleteTodoById(id: string): Promise<Todo> {
    return await this.todoModel.findByIdAndDelete(new Types.ObjectId(id));
  }
}
