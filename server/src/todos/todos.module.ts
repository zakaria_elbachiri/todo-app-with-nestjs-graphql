import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UsersModule } from 'src/users/users.module';
import { TodosService } from './todos.services';
import { TodosResolver } from './todos.resolver';

import { Todo, TodoSchema } from './schemas/todo.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: Todo.name, schema: TodoSchema }]),
    UsersModule,
  ],
  providers: [TodosResolver, TodosService],
})
export class TodosModule {}
