import { ArgsType, Field } from '@nestjs/graphql';

@ArgsType()
export class GetTodoByIdArgs {
  @Field()
  todoId: string;
}

@ArgsType()
export class GetTodosByTypeArgs {
  @Field()
  type: string;
}
