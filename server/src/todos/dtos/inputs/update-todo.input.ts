import { Field, InputType } from '@nestjs/graphql';

@InputType()
export class UpdateTodoInput {
  @Field()
  _id: string;

  @Field()
  type: string;
}
