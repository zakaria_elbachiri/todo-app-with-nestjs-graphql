import { Field, InputType } from '@nestjs/graphql';
import { IsNotEmpty, IsString } from 'class-validator';

@InputType()
export class DeleteTodoInput {
  @Field()
  @IsString()
  @IsNotEmpty()
  todoId: string;
}
