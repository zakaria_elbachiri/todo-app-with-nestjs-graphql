import { InputType, Field } from '@nestjs/graphql';
import { IsOptional, IsNotEmpty } from 'class-validator';

@InputType()
export class CreateTodoInput {
  @IsNotEmpty()
  @Field()
  title: string;

  @Field({ nullable: true })
  @IsOptional()
  description?: string;

  @Field({ defaultValue: 'todo' })
  type: string;

  @Field()
  @IsNotEmpty()
  assignedToUser: string;
}
