import { Field, ObjectType, ID } from '@nestjs/graphql';
import { IsOptional } from 'class-validator';

import { User } from 'src/users/models/user.model';

@ObjectType()
export class Todo {
  @Field(() => ID)
  _id: string;

  @Field()
  title: string;

  @Field({ nullable: true })
  @IsOptional()
  description?: string;

  @Field()
  type: string;

  @Field(() => User)
  assignedToUser: User;
}
