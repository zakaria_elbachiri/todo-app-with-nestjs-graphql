import {
  Args,
  Mutation,
  Parent,
  Query,
  ResolveField,
  Resolver,
} from '@nestjs/graphql';

import { GetTodoByIdArgs, GetTodosByTypeArgs } from './dtos/args/get-tood.args';
import { CreateTodoInput } from './dtos/inputs/create-todo.input';
import { DeleteTodoInput } from './dtos/inputs/delete-todo.input';
import { UpdateTodoInput } from './dtos/inputs/update-todo.input';

import { Todo } from './models/todo.model';

import { TodosService } from './todos.services';
import { UsersService } from 'src/users/users.service';

@Resolver(() => Todo)
export class TodosResolver {
  constructor(
    private todosService: TodosService,
    private usersService: UsersService,
  ) {}

  @Query(() => [Todo], { name: 'todos' })
  async getTodos(): Promise<Todo[]> {
    return this.todosService.getAllTodos();
  }

  @Query(() => Todo, { name: 'todo' })
  async getTodo(@Args() { todoId }: GetTodoByIdArgs): Promise<Todo> {
    return this.todosService.getTodoById(todoId);
  }

  @Query(() => [Todo], { name: 'todosByType' })
  async getTodosByType(@Args() { type }: GetTodosByTypeArgs): Promise<Todo[]> {
    return await this.todosService.getTodosByType(type);
  }

  @Mutation(() => Todo, { name: 'createTodo' })
  async createTodo(
    @Args('createTodoInput')
    { title, description, assignedToUser, type }: CreateTodoInput,
  ) {
    return this.todosService.create(title, description, assignedToUser, type);
  }

  @Mutation(() => Todo, { name: 'updateTodo' })
  async updateTodo(
    @Args('updateTodoInput') { _id, type }: UpdateTodoInput,
  ): Promise<Todo> {
    return await this.todosService.updateTodo(_id, type);
  }

  @Mutation(() => Todo, { name: 'deleteTodo' })
  async deleteTodo(
    @Args('deleteTodoInput') { todoId }: DeleteTodoInput,
  ): Promise<Todo> {
    return this.todosService.deleteTodoById(todoId);
  }

  @ResolveField()
  async assignedToUser(@Parent() { assignedToUser: { _id } }: Todo) {
    return await this.usersService.getUserById(_id);
  }
}
