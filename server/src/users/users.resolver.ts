import { Args, Mutation, Query, Resolver } from '@nestjs/graphql';

import { UsersService } from './users.service';

import { User } from './models/user.model';

import { GetUserArgs } from './dtos/args/get-user.args';
import { CreateUserInput } from './dtos/inputs/create-user.input';

@Resolver(() => User)
export class UsersResolver {
  constructor(private usersService: UsersService) {}

  @Mutation(() => User, { name: 'createUser' })
  createTodo(
    @Args('createUserInput')
    user: CreateUserInput,
  ) {
    return this.usersService.create(user);
  }

  @Query(() => [User], { name: 'users' })
  getTodos() {
    return this.usersService.getAllUsers();
  }

  @Query(() => User, { name: 'user' })
  getUser(@Args() { userId }: GetUserArgs) {
    return this.usersService.getUserById(userId);
  }
}
