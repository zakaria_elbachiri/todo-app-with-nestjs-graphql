import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';

import { User, UserDocument } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private usersModel: Model<UserDocument>,
  ) {}

  async create(user: Object): Promise<User> {
    return await this.usersModel.create({
      _id: new Types.ObjectId(),
      ...user,
    });
  }

  async getAllUsers(): Promise<User[]> {
    return await this.usersModel.find();
  }

  async getUserById(userId: string): Promise<User> {
    return await this.usersModel.findById(new Types.ObjectId(userId));
  }
}
