import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { GraphQLModule } from '@nestjs/graphql';

import { TodosModule } from './todos/todos.module';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot('mongodb://localhost:27017/todo'),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      autoSchemaFile: 'schema.gql',
      // cors: {
      //   origin: 'http://localhost:3000',
      //   credentials: true,
      // },
    }),
    TodosModule,
    UsersModule,
  ],
})
export class AppModule {}
